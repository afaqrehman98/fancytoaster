pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {

    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "FancyToaster"
include(":app")
include(":fancytoaster")
