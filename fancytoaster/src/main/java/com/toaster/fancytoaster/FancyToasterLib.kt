package com.toaster.fancytoaster

import android.content.Context
import android.widget.Toast

object FancyToasterLib {

    fun showFancyToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }
}