plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("maven-publish")
}

android {
    namespace = "com.toaster.fancytoaster"
    compileSdk = 33

    defaultConfig {
        minSdk = 24

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

publishing {
    publications {
        create<MavenPublication>("fancytoaster") {
            // Use components["java"] to reference the Java component
            groupId = "com.toaster.fancytoaster"// Replace with your group ID
            artifactId = "fancytoaster" as String// Replace with your artifact ID
            version = "12.0.0" // Replace with your version
            artifact("$buildDir/outputs/aar/fancytoaster-debug.aar")


            pom.withXml {
                val dependencies = asNode().appendNode("dependencies")
                configurations.getByName("releaseCompileClasspath").getResolvedConfiguration().getFirstLevelModuleDependencies().forEach {
                    val dependency = dependencies.appendNode("dependency")
                    dependency.appendNode("groupId", it.moduleGroup)
                    dependency.appendNode("artifactId", it.moduleName)
                    dependency.appendNode("version", it.moduleVersion)
                }
            }

        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/49026926/packages/maven")
            name = "Gitlab"
            credentials(HttpHeaderCredentials::class) {
                name = "Authorization"
                value = "Bearer ${findProperty("gitLabPrivateToken") as String?}"
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}


dependencies {

    implementation("androidx.core:core-ktx:1.9.0")
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("com.google.android.material:material:1.9.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}